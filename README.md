# vee-validate-rules

## Project setup
```
npm i 'vee-validate-rules'
yarn add 'vee-validate-rules'
```

## Common usage
```
import { extend } from 'vee-validate'
import customRules from 'vee-validate-rules'

import { createApp } from './app'

const { app } = createApp({
  Vue, VueRouter, Vuex, I18n,
})

customRules(app, extend)
```

## Custom messages
```
const messages = {
  emailMethod: app._('Email is not valid')
}

customRules(app, extend, messages)
```

## Rules
```
- emailMethod
- phone
- uaPhone
- username
- latin
- requiredMap
- requiredArray
- arrayMaxLength
- arrayMinLength
- numericMethod
```
