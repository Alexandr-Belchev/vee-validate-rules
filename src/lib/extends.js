import rules from './rules';
import trans from './translates';

/*
  eslint-disable
    no-restricted-syntax
*/

export default function customRules(app, extend, messages = {}) {
  for (const key in rules) {
    if ({}.hasOwnProperty.call(rules, key)) {
      extend(key, {
        ...rules[key],
        message: messages[key] || trans(app)[key],
      });
    }
  }
}
