const trans = app => ({
  emailMethod: app._('The field must be a valid email address.'),
  phone: app._('Phone is not valid.'),
  uaPhone: app._('UA phone is not valid.'),
  username: app._('The field has an invalid format.'),
  latin: app._('The field must contain only latin characters.'),
  requiredMap: app._('This field is required.'),
  requiredArray: app._('This field is required.'),
  arrayMaxLength: app._('The number of elements must be less then {length}.'),
  arrayMinLength: app._('The number of elements must be greater then {length}.'),
  numericMethod: app._('The field must be a number.'),
});

export default trans;
