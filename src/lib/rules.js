import { required, min, max } from 'vee-validate/dist/rules';

const getExistItems = value => {
  const arr = value.filter(el => !el['_delete'])
  return {
    items: arr,
    length: arr.length,
  }
}

/*
  eslint-disable
    no-irregular-whitespace
*/

const rules = {
  emailMethod: {
    validate: () => /^[a-zA-Zа-яА-ЯяёЁЇїІіЄєҐґ\+0-9_.-]{2,64}@[a-zA-Zа-яА-ЯяёЁЇїІіЄєҐґ0-9_.-]+[.a-zA-Zа-яА-ЯяёЁЇїІіЄєҐґ]{0,6}$/,
  },
  phone: {
    validate: () => /^[+ ()-\d]+$/,
  },
  uaPhone: {
    validate: () => /^.\d{12}$/,
  },
  username: {
    validate: () => /^[a-zA-Zа-яА-ЯяёЁЇїІіЄєҐґ ’"-]+$/i,
  },
  latin: {
    validate: () => /^[a-zA-Z \',-\.\:\@\*_!|\^?\'~{}\﻿\+\&\;\"\<\\\>`\n+\(\)\%$#№[\]=0-9\/]+$/,
  },
  requiredMap: {
    validate: (value, keys) => {
      const items = getExistItems(value).items
      const invalid = keys.some(x => !required.validate(items && items[x]).valid);
      return { valid: !invalid, required: min.required || max.required };
    },
  },
  requiredArray: {
    validate(value) {
      const items = getExistItems(value).items
      return items && items.reduce && items.length
        ? items.reduce((acc, x) => {
          const current = required.validate(x);
          return {
            valid: acc.valid && current.valid,
            required: acc.required || current.required,
          };
        }, { valid: true, required: false })
        : required.validate(items);
    },
  },
  arrayMaxLength: {
    validate(value, args) {
      const notDeletedItemsLength = getExistItems(value).length
      return notDeletedItemsLength <= args[0];
    },
    params: ['length'],
  },
  arrayMinLength: {
    validate(value, args) {
      const notDeletedItemsLength = getExistItems(value).length
      return notDeletedItemsLength >= args[0];
    },
    params: ['length'],
  },
  numericMethod: {
    validate(value) {
      return /^[0-9.]*$/i.test(value);
    },
  },
};

export default rules;
